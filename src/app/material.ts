// import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
    MatGridListModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatRadioModule,
    MatDialogModule,
    MatTooltipModule,
    MatTabsModule,
    MatButtonToggleModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressBarModule,
    MatCheckboxModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatListModule,
    MatProgressSpinnerModule
} from '@angular/material';

@NgModule({
    imports: [
        CommonModule,
        MatGridListModule,
        MatButtonModule,
        MatMenuModule,
        MatIconModule,
        MatToolbarModule,
        MatSidenavModule,
        MatFormFieldModule,
        MatInputModule,
        MatExpansionModule,
        MatRadioModule,
        MatDialogModule,
        MatTooltipModule,
        MatTabsModule,
        MatButtonToggleModule,
        MatCardModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressBarModule,
        MatCheckboxModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSelectModule,
        MatListModule,
        MatProgressSpinnerModule
    ],
    exports: [
        MatGridListModule,
        MatButtonModule,
        MatMenuModule,
        MatIconModule,
        MatToolbarModule,
        MatSidenavModule,
        MatFormFieldModule,
        MatInputModule,
        MatExpansionModule,
        MatRadioModule,
        MatDialogModule,
        MatTooltipModule,
        MatTabsModule,
        MatButtonToggleModule,
        MatCardModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressBarModule,
        MatCheckboxModule,
        MatPaginatorModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSelectModule,
        MatListModule,
        MatProgressSpinnerModule

    ],
    declarations: [

    ],
    entryComponents: [

    ],
})
export class MaterialModule { }
